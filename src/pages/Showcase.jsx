import axios from 'axios';
import { Breadcrumb, Placeholder } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import {
  CardLoading,
  FilterCheckbox,
  FilterLoading,
  InputSearch,
  PaginationComponents,
  ProjectCard,
} from '../components';
import Default from '../layouts/Default';
import { HomeIcons } from '../assets';
import { useDispatch, useSelector } from 'react-redux';
import {
  getSectionsFilter,
  getStylesFilter,
  resetFilter,
} from '../redux/actionCreators/filterAction';
import { useEffect, useState } from 'react';
import { scrollToTop } from '../utility/scroll';

const Showcase = () => {
  const { sections, styles, search } = useSelector((state) => state.filter);
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const [contents, setContents] = useState([]);
  const [pages, setPages] = useState(0);
  const token = localStorage.getItem('token');

  useEffect(() => {
    scrollToTop();
  }, []);

  const getShowcase = ({ page, sections, styles }) => {
    if (!page) {
      page = 1;
    }

    if (token) {
      axios({
        method: 'get',
        url: `${process.env.REACT_APP_BASE_API}showcase`,
        params: {
          page,
          section: sections,
          styles,
          keywords: search,
        },
        headers: {
          Authorization: token,
        },
      })
        .then((response) => {
          setContents(response.data.result);
          setPages(response.data.jumlahPage);
        })
        .then(() => {
          setIsLoading(false);
        });
    } else {
      axios({
        method: 'get',
        url: `${process.env.REACT_APP_BASE_API}showcase`,
        params: {
          page,
          section: sections,
          styles,
          keywords: search,
        },
      })
        .then((response) => {
          setContents(response.data.result);
          setPages(response.data.jumlahPage);
        })
        .then(() => {
          setIsLoading(false);
        });
    }
  };

  useEffect(() => {
    dispatch(getSectionsFilter());
    dispatch(getStylesFilter());
    getShowcase({ page: 1 });
    // eslint-disable-next-line
  }, [dispatch]);

  const applyFilter = () => {
    const filterSections = sections.result
      .filter((section) => section.value === true)
      .map((section) => section.name)
      .join();

    const filterStyles = styles.result
      .filter((style) => style.value === true)
      .map((style) => style.name)
      .join();

    getShowcase({ sections: filterSections, styles: filterStyles });
  };

  const isFiltered =
    sections?.result?.find((arr) => arr.value === true) ||
    styles?.result?.find((arr) => arr.value === true);

  return (
    <>
      <Default>
        <div className='Showcase container py-1'>
          <Breadcrumb className='mt-4 py-3'>
            <span className='text-ash me-3'>
              <HomeIcons />
            </span>
            <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/' }}>
              Home
            </Breadcrumb.Item>
            <Breadcrumb.Item active>Showcase</Breadcrumb.Item>
          </Breadcrumb>
          {isLoading ? (
            <>
              <Placeholder as='h2' animation='glow' className='w-100 mb-4'>
                <Placeholder xs={7} />
                <br />
                <Placeholder xs={4} />
              </Placeholder>
              <Placeholder as='p' animation='glow mb-5'>
                <Placeholder xs={5} />
              </Placeholder>
              <div className='showcase-wrapper row'>
                <div className='col-3 mt-2 filter'>
                  <Placeholder as='h5' animation='glow'>
                    <Placeholder xs={6} />
                  </Placeholder>
                  <FilterLoading />
                </div>

                {/* Placeholder Input Search */}
                <div className='projects col d-flex flex-column'>
                  <Placeholder
                    as='h3'
                    xs={6}
                    animation='glow'
                    className='search mb-4 ps-lg-3 w-100 d-flex justify-content-end'>
                    <Placeholder xs={12} lg={6} />
                  </Placeholder>
                  <div className='row g-4 mb-4'>
                    <CardLoading />
                    <CardLoading />
                    <CardLoading />
                    <CardLoading />
                  </div>
                  <div className='d-flex justify-content-end mb-5'>
                    <PaginationComponents />
                  </div>
                </div>
              </div>
            </>
          ) : (
            <>
              <h2 className='h1 fw-bold serif'>
                Nisi, pellentesque ullamcorper enim libero, fusce sit nulla
                maecenas.
              </h2>
              <p className='text-ash mb-5'>
                Nisi, pellentesque ullamcorper enim libero, fusce sit nulla
                maecenas.
              </p>
              <div className='showcase-wrapper row'>
                <div className='col-3 mt-2 filter'>
                  {isFiltered ? (
                    <div className='d-flex justify-content-between'>
                      <button
                        className='btn btn-link text-decoration-underline mb-2 px-0'
                        onClick={() =>
                          dispatch(resetFilter({ sections, styles }))
                        }>
                        Remove Filter
                      </button>
                      <button
                        className='btn btn-primary mb-2'
                        onClick={applyFilter}>
                        Apply
                      </button>
                    </div>
                  ) : (
                    <h5 className='fw-bold fs-6'>All Projects</h5>
                  )}
                  <FilterCheckbox title='Sections' data={sections.result} />
                  <FilterCheckbox title='Styles' data={styles.result} />
                </div>
                <div className='projects col'>
                  <div className='search ms-auto mb-3 ps-lg-3 w-50'>
                    <InputSearch onSubmit={() => getShowcase({ page: 1 })} />
                  </div>
                  <div className='row g-4 mb-4'>
                    {contents &&
                      contents.map((content, idx) => (
                        <ProjectCard
                          data={content}
                          key={idx}
                          getShowcase={getShowcase}
                        />
                      ))}
                  </div>
                  <div className='d-flex justify-content-end mb-5'>
                    <PaginationComponents
                      pages={pages}
                      getShowcase={getShowcase}
                    />
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </Default>
    </>
  );
};

export default Showcase;
