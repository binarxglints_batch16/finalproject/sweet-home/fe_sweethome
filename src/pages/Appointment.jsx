import { useState } from "react";
import { Container, Nav, Tab } from "react-bootstrap";
import { Outlet } from "react-router-dom";
import {
  AppointmentDate,
  AppointmentReviews,
  FormEnquiryDetails,
} from "../components";
import Default from "../layouts/Default";

const Appointment = () => {
  const [activeTab, setActiveTab] = useState("enquirydetails");
  const [dateTab, setDateTab] = useState(false);
  const [reviewTab, setReviewTab] = useState(false);

  return (
    <Default>
      <div className="Appointment"></div>
      <div className="Appointment pt-3">
        <header className="bg-secondary">
          <Container className="py-5">
            <h3 className="fw-bold">New Appointment</h3>
            <p>Get free professional consultation, Rhoncus sed at nulla odio</p>
          </Container>
        </header>
        <Tab.Container
          id="appointment-form"
          activeKey={activeTab}
          onSelect={(k) => setActiveTab(k)}
        >
          <div className="bg-secondary">
            <Nav
              justify
              variant="tabs"
              defaultActiveKey="enquirydetails"
              className="bg-secondary container row mx-auto"
            >
              <Nav.Item className="col h-100">
                <Nav.Link className="fw-bold" eventKey="enquirydetails">
                  Enquiry Details
                </Nav.Link>
              </Nav.Item>
              <Nav.Item className="col h-100">
                <Nav.Link
                  className="fw-bold"
                  eventKey="appointmentdate"
                  disabled={!dateTab}
                >
                  Appointment Date
                </Nav.Link>
              </Nav.Item>
              <Nav.Item className="col h-100">
                <Nav.Link
                  className="fw-bold h-100"
                  eventKey="reviews"
                  disabled={!reviewTab}
                >
                  Reviews
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </div>
          <Tab.Content>
            <Tab.Pane eventKey="enquirydetails" className="container">
              <FormEnquiryDetails
                onSubmit={() => {
                  setActiveTab("appointmentdate");
                  setDateTab(true);
                }}
              />
            </Tab.Pane>
            <Tab.Pane eventKey="appointmentdate" className="container">
              <AppointmentDate
                onSubmit={() => {
                  setActiveTab("reviews");
                  setReviewTab(true);
                }}
              />
            </Tab.Pane>
            <Tab.Pane eventKey="reviews" className="container">
              <AppointmentReviews />
            </Tab.Pane>
          </Tab.Content>
          <Outlet />
        </Tab.Container>
      </div>
    </Default>
  );
};

export default Appointment;
