import { Route, Routes } from "react-router-dom";
import {
  AppointmentDate,
  AppointmentReviews,
  FormEnquiryDetails,
} from "./components";
import {
  HomePage,
  Showcase,
  DashboardPage,
  NotFoundPages,
  Appointment,
  Project,
} from "./pages";
import PrivateRoute from "./privateRoutes";

function App() {
  return (
    <Routes>
      <Route index element={<HomePage />} />
      <Route path="showcase" element={<Showcase />} />

      <Route path="project/:id" element={<Project />} />
      <Route path="*" element={<NotFoundPages />} />

      <Route path="" element={<PrivateRoute />}>
        <Route path="dashboard" element={<DashboardPage />} />
        <Route path="appointment" element={<Appointment />}>
          <Route path="enquirydetails" element={<FormEnquiryDetails />} />
          <Route path="date" element={<AppointmentDate />} />
          <Route path="reviews" element={<AppointmentReviews />} />
        </Route>
      </Route>
    </Routes>
  );
}

export default App;
