import React, { useEffect } from 'react';
import { Outlet, useNavigate } from 'react-router-dom';
import { useLoginModal } from '../context/LoginContext';

const PrivateRoute = () => {
  const { setShowLoginModal } = useLoginModal();
  const navigate = useNavigate();

  useEffect(() => {
    const token = window.localStorage.getItem('token');

    if (!token) {
      setShowLoginModal(true);
      navigate('/');
    }
    // eslint-disable-next-line
  }, []);
  return <Outlet />;
};

export default PrivateRoute;
