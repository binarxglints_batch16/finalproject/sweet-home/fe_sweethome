import { LOGIN_USER, SET_USER } from '../constants';
import axios from 'axios';

export const LoginUser = (data) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({
      type: LOGIN_USER,
      payload: {
        loading: true,
        result: null,
        error: false,
      },
    });
    axios({
      method: 'post',
      url: `${process.env.REACT_APP_BASE_API}user/login`,
      data: data,
    })
      .then((response) => {
      //   console.log(response);
        window.localStorage.setItem('token', response.data.result.token);
        dispatch({
          type: LOGIN_USER,
          payload: {
            loading: false,
            result: response.data.result,
            error: false,
          },
        });
        resolve(response.data.result.token);
      })
      .catch((error) => {
        dispatch({
          type: LOGIN_USER,
          payload: {
            loading: false,
            result: null,
            error: error.response.data.message,
          },
        });
        reject(error.response.data.message);
      });
  });
};

export const setUser = (data) => (dispatch) => {
  dispatch({
    type: SET_USER,
    payload: {
      loading: true,
      result: null,
      error: false,
    },
  });
  axios({
    method: 'post',
    url: `${process.env.REACT_APP_BASE_API}user/register`,
    data: data,
  }).then((response) => {
   //  console.log(response);
    dispatch({
      type: SET_USER,
      payload: {
        loading: false,
        result: response,
        error: false,
      },
    });
  });
};

