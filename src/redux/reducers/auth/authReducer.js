import { LOGIN_USER, SET_USER } from '../../constants';

const initialState = {
  loginUser: {
    loading: false,
    result: null,
    error: false,
  },
  user: {
    loading: false,
    result: null,
    error: false,
  },
  token: '',
};

const auth = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case LOGIN_USER:
      return {
        ...state,
        loginUser: {
          loading: payload.loading,
          result: payload.result,
          error: payload.error,
        },
      };
    case SET_USER:
      return {
        ...state,
        user: {
          loading: payload.loading,
          result: payload.result,
          error: payload.error,
        },
      };
    // case USER_LOGOUT:
    //    return {
    //       ...state,
    //       user: {
    //          loading: false,
    //          result: null,
    //          error: false,
    //       },
    //       token:""
    //    };
    // case TOKEN:
    //    return {
    //       ...state,
    //       token: action.payload,
    //    };
    default:
      return state;
  }
};

export default auth;
