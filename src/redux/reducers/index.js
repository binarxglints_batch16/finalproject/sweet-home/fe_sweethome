import { combineReducers } from 'redux';
import appointmentForm from './appointment/appointmentForm';
import filterReducer from './filterShowcase/filterReducer';
import userReducer from './user/userReducer';
import projectDetailReducer from './Project/projectDetailReducers';
import authReducer from "./auth/authReducer";

const rootReducer = combineReducers({
  filter: filterReducer,
  appointment: appointmentForm,
  user : userReducer,
  project : projectDetailReducer,
  auth: authReducer,
});

export default rootReducer;
