import {
   SET_USER,
} from '../../constants';

const initialState = {
   user: {
      loading: false,
      data: false,
      error: false,
    },
 };
 
 const userReducer = (state = initialState, action) => {
   const { type, payload } = action;
   switch (type) {
     case SET_USER:
       return {
         ...state,
         user: {
            loading: payload.loading,
            data: payload.data,
            error: payload.error,
          },
       };
     default:
       return state;
   }
 }

 export default userReducer;