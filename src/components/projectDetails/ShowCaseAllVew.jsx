import React from 'react'
import {ImageCard} from '../../components'
import { Modal } from 'react-bootstrap'


const ShowCaseAllVew = (props) => {
  
   const image = props.image;


  return (
    <div className=" card-content ">
      <Modal {...props}>
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            All Photos
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row g-3 d-flex flex-wrap pt-2">
             {(image.map((image, index)=>{return(
            <div className="col-3" key={index}>
              <ImageCard
                image={image.picture}
                title="Living Room"
                classTitle="mb-0 fs-6 fw-bold"
                classOverlay="d-flex align-items-end"
              />
            </div>
             )}))}
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ShowCaseAllVew