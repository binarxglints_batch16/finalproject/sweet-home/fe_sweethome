import { HomeIcons } from '../../assets';
import { Button } from 'react-bootstrap';
import { rupiahFormatter } from '../../utility/number';
import { useNavigate } from 'react-router-dom';
import icon from '../../assets/icons/svg/plus.svg';

const PropertyDetails = (props)=>{

   const style = props.style;
   const include = props.include;
   const project = props.project;
   const navigate = useNavigate();
   const isLogin = localStorage.getItem('token');

   const direct = () => {
      navigate('/appointment');
    };
   // console.log(style);
   // console.log(include);
   // console.log(project)

   return(
      <div className="propertyDetails">
         <div className="propertyDetail row bg-wheat py-3 px-2 mx-1 mb-3 rounded-1">
            <div className="row">
               <div className="col">
                  {/* this is line for Property Name */}
                  {project ? (
                        <>
                        <span className="propType">Property Type</span>
                        <p className="propName fw-bold text-primary">{project.appointment.buildingType.name}</p>
                        </>
                     ):(
                        <>
                        <span className="propType">Property Type</span>
                        <p className="propName fw-bold text-primary"></p>
                        </>
                     )
                  }
               </div>
               <div className="col">
                  {/* this is line for Area Size */}
                  {project ? (
                     <>
                     <span className="areaSize">Area Size</span>
                     <p className="Size fw-bold text-primary">{project.totalArea}</p>
                     </>
                  ):(
                     <>
                     <span className="areaSize">Area Size</span>
                     <p className="Size fw-bold text-primary"></p>
                     </>
                  )}
               </div>
            </div>
            <div className="row">
               <div className="col">
                  {/* this is line for Project Cost */}
                  {project ? (
                     <>
                     <span className="cost">Cost</span>
                     <p className="propName fw-bold text-primary">
                        {rupiahFormatter(parseInt(project.totalPrice)).replace(',00', '')}
                     </p>
                     </>
                  ):(
                     <>
                     <span className="cost">Cost</span>
                     <p className="propName fw-bold text-primary"></p>
                     </>
                  )}
               </div>
               <div className="col">
                  {/* this is line for Project Duration */}
                  {project ? (
                     <>
                     <span className="duration">Renovation Duration</span>
                     <p className="durationTime fw-bold text-primary">{`${project.totalDuration} Weeks`}</p> 
                     </>
                  ):(
                     <>
                     <span className="duration">Renovation Duration</span>
                     <p className="durationTime fw-bold text-primary"></p>
                     </>
                  )}
               </div>
            </div>
            <div className="row">
               <span className="style">Style</span>
               <div className="d-flex">
                  {/* this is line for style category */}
                  {(style.map((style, index)=>{return(
                     <p className="styles fw-bold text-primary pe-1 mb-0" key={index}>
                        {`${style.style.name},`}
                     </p>
                  )}))}
               </div>
            </div>
         </div>
         <div className="projectInclude d-flex row bg-wheat py-3 px-2 mx-1 mb-3 rounded-1">
            <div className="includeHead row m-0 p-0 fw-bold text-primary border-bottom border-1 border-ash">
               <div className="col ">
                  Work Include
               </div>
            </div>
            <div className="includeContent pt-2 row d-flex">
               {/* this is line for work include */}
               {(include.map((include, index)=>{return(
                  <div className="col-6" key={index}>
                     {`- ${include.projectType.name}`}
                  </div>
               )}))}
            </div>
         </div>
         {isLogin &&(
         <div className="getQuotes row  bg-wheat py-3 px-2 mx-1 mb-3 rounded-1">
            <div className="quoteSection text-center">
               <HomeIcons/>
               <p>Let Our Profesional Designer Visualize Your Dream House</p>
               <Button 
                  variant="primary" 
                  className="fw-bold px-5"
                  onClick={() => direct()}
               >
                  <img src={icon} alt='add icon' />
                  <span className='ps-3'>Create Appointment</span>
               </Button>
            </div>
         </div>
         )}
      </div>
   )
}

export default PropertyDetails;