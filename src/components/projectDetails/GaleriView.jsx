import { Modal, Carousel, CloseButton } from 'react-bootstrap';
import {useState} from 'react';

const GaleriView = (props)=>{
   const [index, setIndex] = useState(0);

   const handleSelect = (selectedIndex, e) => {
     setIndex(selectedIndex);
   };

   const image = props.image;
   // console.log(image);

   return(
      <div className="galeriView">
         <Modal
         {...props}
         >
            <Modal.Header className="border-0 bg-dark text-end">
            <CloseButton variant="white" onClick ={props.onHide} />
            </Modal.Header>
            <Modal.Body className="bg-dark">
               <Carousel activeIndex={index} onSelect={handleSelect}>
                  {(image.map((image, index)=>{return(
                  <Carousel.Item className="text-center" key={index}>
                  <img
                     className="d-block w-75 mx-auto"
                     src={image.picture}
                     alt="First slide"
                  />
                  </Carousel.Item>
                  )}))}
               </Carousel>
            </Modal.Body>
         </Modal>
      </div>
   )
}

export default GaleriView;