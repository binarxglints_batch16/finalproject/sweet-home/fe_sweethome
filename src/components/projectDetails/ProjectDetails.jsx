import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { HeartIcons } from '../../assets';
import { PropertyDetails, ImageCard, Loading } from '../../components';
import { getProjectDetail } from '../../redux/actionCreators/projectDetailActios';
import { Row, Col } from 'react-bootstrap';
import axios from 'axios';
import GaleriView from './GaleriView';
import ShowCaseAllVew from './ShowCaseAllVew';

const ProjectDetails = () => {
  const [showAllVew, setShowAllVew] = useState(false);
  const [show, setShow] = useState(false);
  const [favorite, setFavorite] = useState();
  const dispatch = useDispatch();
  const isLogin = localStorage.getItem('token');
  const projectDetail = useSelector((state) => state.project.project);
  const loading = useSelector((state) => state.project.loading);
  const params = useParams();
  
  const getFavorite = (id) => {
     axios({
        method : 'GET',
        url: `${process.env.REACT_APP_BASE_API}showcase/${id}`,
        headers: {
           Authorization: localStorage.getItem('token'),
         }
      })
      .then((response)=>{
         console.log(response)
         const data = response.data.data[1].IsFavorite
         setFavorite(data)
      })
   }
   
   const toggleFavorite = (id) => {
    axios({
      method: favorite ? 'delete' : 'post',
      url: `${process.env.REACT_APP_BASE_API}favorite/${id}`,
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    })
    .then(()=>{
      getFavorite(params.id)
    })
  };

  useEffect(() => {
    dispatch(getProjectDetail(params.id));
    getFavorite(params.id)
    // eslint-disable-next-line
  }, []);

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <div className='projectDetails'>
          <Row className='row pb-4'>
            <Col md={7} xs={12}>
              <div className='projectTitle fs-3 fw-bold'>
                {projectDetail[0].name}
              </div>
              {!projectDetail[0].project ? (
                <div className='projectLocation'>
                </div>
              ) : (
                  <div className='projectLocation'>
                     {projectDetail[0].project.appointment.address}
                  </div>  
              )}
            </Col>
            {isLogin && (
               <Col className='d-flex align-items-center justify-content-end' onClick={() => toggleFavorite(params.id)}>
                  <div className='favouriteButton px-2 py-1'>
                     <HeartIcons
                        fill={`${favorite ? '#DD5571' : 'white'}`}
                        stroke={`${!favorite && '#214457'}`}
                     />
                     <span className='text-primary fw-bold ps-3'>
                        Add to Favourite
                     </span>
                  </div>
               </Col>
            )}
          </Row>
          <Row className='projectImage row mx-auto'>
            <Col lg={7} xs={12}>
              <div className='row pb-3'>
                <div
                  className='mainImage col me-2'
                  onClick={() => setShow(true)}
               >
                  <ImageCard
                    image={projectDetail[0].gallery[0].picture}
                    title={projectDetail[0].gallery[0].title}
                    classTitle='mb-0 fs-6 fw-bold'
                    classOverlay='d-flex align-items-end'
                  />
                </div>
              </div>
              {projectDetail[0].gallery.length > 1 && (
                <div className='row d-flex flex-wrap pt-2'>
                  <div 
                     className='childImage col' 
                     onClick={() => setShow(true)}
                  >
                    <ImageCard
                      image={projectDetail[0].gallery[1].picture}
                      title={projectDetail[0].gallery[1].title}
                      classTitle='mb-0 fs-6 fw-bold'
                      classOverlay='d-flex align-items-end'
                    />
                  </div>
                  <div 
                     className='childImage col' 
                     onClick={() => setShow(true)}
                  >
                    <ImageCard
                      image={projectDetail[0].gallery[2].picture}
                      title={projectDetail[0].gallery[2].title}
                      classTitle='mb-0 fs-6 fw-bold'
                      classOverlay='d-flex align-items-end'
                    />
                  </div>
                  <div
                    className='allImage col'
                    onClick={() => setShowAllVew(true)}
                  >
                    <ImageCard
                      image={projectDetail[0].gallery[3].picture}
                      title={`View All`}
                      classTitle='mb-0 fs-6 text-dark fw-bold text-center text-wrap w-50'
                      classOverlay='d-flex bg-secondary bg-opacity-75 justify-content-center align-items-center'
                    />
                  </div>
                </div>
              )}
            </Col>
            <Col>
              <PropertyDetails
                project={projectDetail[0].project}
                style={projectDetail[0].showcaseJunkstyle}
                include={projectDetail[0].showcaseJunkProjectType}
              />
            </Col>
            <div className='showAllVew'>
              <ShowCaseAllVew
                show={showAllVew}
                onHide={() => setShowAllVew(false)}
                dialogClassName='modal-fullscreen'
                aria-labelledby='example-custom-modal-styling-title'
                image={projectDetail[0].gallery}
              />
            </div>
          </Row>
          <div className='showImageModal'>
            <GaleriView
              show={show}
              onHide={() => setShow(false)}
              dialogClassName='modal-fullscreen'
              aria-labelledby='example-custom-modal-styling-title'
              image={projectDetail[0].gallery}
            />
          </div>
        </div>
      )}

    </>
  );
};

export default ProjectDetails;
