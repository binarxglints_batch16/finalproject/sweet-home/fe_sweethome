import kitchen from '../../assets/kitchen.jpg';
import { useNavigate } from 'react-router-dom';

const ProductHighlight = () => {
  const image = kitchen;
  const navigate = useNavigate();
  
  const direct = () => {
    navigate('/showcase');
  };


  return (
    <div className='productHighlight d-flex justify-content-center'>
      <div className='contentImage card mx-auto'>
        <img
          src={image}
          className='card-img imageHighlight vw-100 vh-100'
          alt='...'
        />
        <div className='contentOverlay d-flex align-items-end card-img-overlay'>
          <div className='ideasBar d-flex bg-primary'>
            <div className='categoryFilter d-flex align-items-center justify-content-between m-auto text-light'>
              <div className='ideaCaption'>
                Get currated renovation idea for your dream home
              </div>
              <div className='seeAllButton'>
                <button
                  type='submit'
                  className='btn px-4 btn-secondary fw-bold'
                  onClick={() => direct()}>
                  See All
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductHighlight;
