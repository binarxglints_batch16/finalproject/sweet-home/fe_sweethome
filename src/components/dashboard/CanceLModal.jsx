import { Modal, Form, Button } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';
import axios from 'axios';

const CancelModal = (props) => {
  const [cancel, setCancel] = useState();
  const data = new URLSearchParams();
  data.append('reasonCancel', cancel);

  const createCancel = (id) => {
    axios({
      method: 'post',
      url: `${process.env.REACT_APP_BASE_API}project/requestcancel/${id}`,
      headers: {
        Authorization: localStorage.getItem('token'),
      },
      data,
    })
      .then((response) => {
        console.log(response);
        Swal.fire({
          icon: 'success',
          title: 'SUCCESS',
          text: 'Your request has been submited',
        });
        props.reload();
      })
      .catch((error) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error.response.data.message,
        });
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    createCancel(props.id);
    props.onHide();
  };

  return (
    <Modal
      {...props}
      size='md'
      aria-labelledby='contained-modal-title-vcenter'
      centered>
      <Modal.Header className='border-0' closeButton>
        <Modal.Title id='contained-modal-title-vcenter' className='fw-bold'>
          Request Cancellation
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Label className='fw-bold'>Project Id</Form.Label>
          <Form.Control
            className='mb-4 w-50'
            type='text'
            placeholder={props.code}
            disabled={true}
          />
          <Form.Label className='fw-bold'>Reason</Form.Label>
          <Form.Control
            className='mb-3'
            as='textarea'
            rows={5}
            onChange={(e) => setCancel(e.target.value)}
            required
          />
          <div className='button text-center'>
            <Button
              type='submit'
              // onClick={props.onHide}
              variant='secondary'
              className='text-dark fw-bold px-5 mb-3'>
              Submit Request
            </Button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  );
};
export default CancelModal;
