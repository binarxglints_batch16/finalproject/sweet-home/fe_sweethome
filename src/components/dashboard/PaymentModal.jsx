import { Modal, Form, Button } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';
import axios from 'axios';

const PaymentModal = (props) => {
  const [payment, setPayment] = useState();
  const [selectedFile, setSelectedFile] = useState();

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const data = new FormData();
  data.append('uploadReceipt', selectedFile);
  data.append('noteUploadReceipt', payment);

  const paymentConfirm = (id) => {
    axios({
      method: 'post',
      url: `${process.env.REACT_APP_BASE_API}project/receipt/${id}`,
      headers: {
        Authorization: localStorage.getItem('token'),
      },
      data,
    })
      .then(() => {
        Swal.fire({
          icon: 'success',
          title: 'SUCCESS',
          text: 'Your payment has been uploaded',
        });
        props.reload()
      })
      .catch((error) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error.response.data.message,
        });
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    paymentConfirm(props.id);
    props.onHide();
  };

  return (
    <Modal
      {...props}
      size='md'
      aria-labelledby='contained-modal-title-vcenter'
      centered>
      <Modal.Header className='border-0' closeButton>
        <Modal.Title id='contained-modal-title-vcenter' className='fw-bold'>
          <div className='fw-bold'>Payment Confirmation</div>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Label className='fw-bold'>Upload Receipt</Form.Label>
          <Form.Control
            className='mb-4'
            type='file'
            placeholder='Masukan Project Id'
            onChange={changeHandler}
          />
          <Form.Label className='fw-bold'>Note</Form.Label>
          <Form.Control
            className='mb-3'
            as='textarea'
            rows={5}
            value={payment}
            onChange={(e) => setPayment(e.target.value)}
          />
          <div className='button text-center'>
            <Button
              type='submit'
              // onClick={props.onHide}
              variant='secondary'
              className='text-dark fw-bold px-5 mb-3'>
              Submit Receipt
            </Button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  );
};
export default PaymentModal;
