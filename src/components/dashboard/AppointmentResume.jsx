import { Accordion, Row, Col } from "react-bootstrap";
import { useState, useEffect } from "react";
import axios from "axios";
import moment from "moment";
import { Loading } from "../../components";
import { color, appointmentStatus } from "../../utility/status";
import { rupiahFormatter } from "../../utility/number";
import EmptySection from "../blank/EmptySection";

const AppointmentResume = () => {
  const token = localStorage.getItem("token");
  const [appointment, setAppointment] = useState({
    loading: true,
    data: false,
    error: false,
  });

  useEffect(() => {
    axios({
      method: "get",
      url: `${process.env.REACT_APP_BASE_API}appointment`,
      headers: {
        Authorization: token,
      },
    })
      .then((response) => {
        setAppointment({
          loading: false,
          data: response.data,
          error: false,
        });
      })
      .catch((error) => {
        setAppointment({
          loading: false,
          data: null,
          error: error.message,
        });
      });
    // eslint-disable-next-line
  }, []);

  const data = appointment.data;

  return (
    <>
      {appointment.data && appointment.data.length <= 0 && <EmptySection quote='appointment' />}
      {appointment.loading ? (
        <Loading />
      ) : (
        <div className="appointmentResume">
          <Accordion>
            {data.map((data, index) => {
              return (
                <Accordion.Item
                  className={`border-${color(
                    data.status
                  )} shadow p-3 mb-5 bg-body`}
                  eventKey={index}
                  key={index}
                >
                  <Accordion.Header>
                    <Row className="d-flex w-100">
                      <Row className="align-items-center mb-2">
                        <Col md={6} xs={12}>
                          <div
                            className={`status badge px-3 fs-6 rounded-pill bg-${color(
                              data.status
                            )}`}
                          >
                            {appointmentStatus(data.status)}
                          </div>
                        </Col>
                        <Col>
                          <div className="date text-ash">
                            {`Created on ${moment(data.createdAt).format(
                              "LL"
                            )}`}
                          </div>
                        </Col>
                      </Row>
                      <Row className="d-flex pt-3">
                        <Col xs={12}>
                          <div className="d-flex scheduleDate">
                            <p className="mb-0 pe-2 fw-bold">
                              {data.appointmentDate}
                            </p>
                            <span className="vr"></span>
                            <p className="mb-0 ps-2 text-ash">
                              {data.timeslot.time}
                            </p>
                          </div>
                        </Col>
                      </Row>
                    </Row>
                  </Accordion.Header>
                  <Accordion.Body>
                    <Row className="detail d-flex w-100 ">
                      <Col md={6} xs={12} className="aptDetail mb-4">
                        <div className="fw-bold">Appointment Details</div>
                        <div className="d-flex pt-4">
                          <div className="w-50">
                            <div className="text-ash">Building Type</div>
                            <div className="fw-bold">
                              {data.buildingType.name}
                            </div>
                          </div>
                          <div className="w-50">
                            <div className="text-ash">Service Type</div>
                            <div className="fw-bold">
                              {data.serviceType.name}
                            </div>
                          </div>
                        </div>
                        <div className="d-flex pt-4">
                          <div className="w-50">
                            <div className="text-ash">
                              Estimated Work Duration
                            </div>
                            <div className="fw-bold">{`${data.estimateTime} Month`}</div>
                          </div>
                          <div className="w-50">
                            <div className="text-ash">Budget</div>
                            <div className="fw-bold">
                              {rupiahFormatter(parseInt(data.budget)).replace(
                                ",00",
                                ""
                              )}
                            </div>
                          </div>
                        </div>
                        <div className="pt-4">
                          <div className="text-ash">Address</div>
                          <div className="fw-bold">{data.address}</div>
                        </div>
                        <div className="pt-4">
                          <div className="text-ash">Inspection Area</div>
                          <div className="fw-bold">
                            {data.section.map((section, index) => {
                              return <span key={index}>{`${section}, `}</span>;
                            })}
                          </div>
                        </div>
                      </Col>
                      <Col md={6} xs={12} className="note">
                        <div className="fw-bold">Note</div>
                        <div className="pt-4">{data.note}</div>
                      </Col>
                    </Row>
                  </Accordion.Body>
                </Accordion.Item>
              );
            })}
          </Accordion>
        </div>
      )}
    </>
  );
};

export default AppointmentResume;
