import React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Liked } from '../../assets';
import { rupiahFormatter } from '../../utility/number';
import axios from 'axios';

const FavouriteCard = (props) => {
   const data = props.data;

  const dislike = (id) => {
    axios({
      method: 'delete',
      url: `${process.env.REACT_APP_BASE_API}favorite/${id}`,
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    }).then(() => {
      props.reload()
    });
  };

  return (
    <>
      {data.map((data, index) => {
        return (
               <div className='col-12 col-md-6' key={index}>
                  <div className='ProjectCard card text-decoration-none text-dark shadow-sm'>
                  <div
                     className='icon-favorite text-end'
                     onClick={() => dislike(data.showcaseId)}>
                     <Liked />
                  </div>
                  <Link to={`/project/${data.showcaseId}`}>
                     <Card.Img variant='top' src={data.showcase.gallery[0].picture} />
                     <Card.Body>
                        <div className='d-flex justify-content-between'>
                           <Card.Title className='fw-bold text-truncate mb-0'>
                              {data.showcase.name}
                           </Card.Title>
                           {data.showcase.project ? (
                              <Card.Text className='estimated-time mb-0'>
                                 {data.showcase.project.totalDuration}
                              </Card.Text>
                           ) : (
                              <Card.Text className='estimated-time mb-0'></Card.Text>
                           )}
                        </div>
                        <div className='d-flex justify-content-between'>
                        {data.showcase.project ? (
                           <Card.Text className='mb-0'>
                              {data.showcase.project.appointment.address}
                           </Card.Text>
                        ) : (
                           <Card.Text className='mb-0'></Card.Text>
                        )}
                        {data.showcase.project ? (
                           <Card.Text>
                              {rupiahFormatter(
                                 parseInt(data.showcase.project.totalPrice)
                              ).replace(',00', '')}
                           </Card.Text>
                        ) : (
                           <Card.Text></Card.Text>
                        )}
                        </div>
                     </Card.Body>
                  </Link>
                  </div>
               </div>
        );
      })}
    </>
  );
};

export default FavouriteCard;
