import { createContext, useState, useContext } from "react";

const LoginContext = createContext();

// Context untuk modal Login
function LoginModalContextProvider(props) {
  const [showLoginModal, setShowLoginModal] = useState(false);

  return (
    <LoginContext.Provider value={{ showLoginModal, setShowLoginModal }}>
      {props.children}
    </LoginContext.Provider>
  );
}

// Custom Hooks untuk Modal Login
export function useLoginModal() {
  return useContext(LoginContext);
}

export default LoginModalContextProvider;
